<?php

/**
 * Visual Paginator for Nette framework
 *
 * @author Jiří Novák <novak@egen.cz>
 */

namespace eGen\Controls;

use Nette\Application\UI\Control;
use Nette\Localization\ITranslator;
use Nette\Utils\Paginator;

class VisualPaginator extends Control
{

	/** @var Paginator */
	private $paginator;

	/** @var ITranslator */
	private $translator;

	/* @var bool */
	private $useAjax = FALSE;

	/** @var string|NULL */
	private $templateFile;

	/** @persistent */
	public $page = 1;

	/**
	 * @return Paginator
	 */
	public function getPaginator()
	{
		if(!$this->paginator) {
			$this->paginator = (new Paginator)->setBase(1);
		}
		return $this->paginator;
	}

	/**
	 * @return ITranslator|NULL
	 */
	public function getTranslator()
	{
		return $this->translator;
	}

	/**
	 * @param ITranslator|NULL $translator
	 */
	public function setTranslator(ITranslator $translator = NULL)
	{
		$this->translator = $translator;
	}

	/**
	 * @return $this
	 */
	public function enableAjax()
	{
		$this->useAjax = TRUE;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function disableAjax()
	{
		$this->useAjax = FALSE;
		return $this;
	}

	/**
	 * @param string $templateFile
	 * @return $this
	 */
	public function setTemplateFile($templateFile)
	{
		$this->templateFile = $templateFile;
		return $this;
	}

	/**
	 * @return void
	 */
	public function render()
	{
		if($this->templateFile) {
			$this->template->setFile($this->templateFile);
		}
		elseif(!$this->template->getFile()) {
			$this->template->setFile(__DIR__ . '/template.latte');
		}

		if($this->getTranslator() instanceof ITranslator) {
			$this->template->setTranslator($this->getTranslator());
		}

		if($this->useAjax) {
			$this->redrawControl('paginator');
		}

		$this->template->paginator = $this->getPaginator();
		$this->paginator->page = $this->page;
		$this->template->steps = $this->getSteps();
		$this->template->useAjax = $this->useAjax;
		$this->template->render();
	}

	/**
	 * @return array
	 */
	private function getSteps()
	{
		$paginator = $this->getPaginator();

		if ($paginator->pageCount < 2) {
			$steps = array($paginator->page);
		}
		else {
			$arr = range(max($paginator->firstPage, $paginator->page - 3), min($paginator->lastPage, $paginator->page + 3));
			$count = 4;
			$quotient = ($paginator->pageCount - 1) / $count;
			for ($i = 0; $i <= $count; $i++) {
				$arr[] = round($quotient * $i) + $paginator->firstPage;
			}
			sort($arr);
			$steps = array_values(array_unique($arr));
		}

		return $steps;
	}

	/**
	 * {@inheritdoc}
	 */
	public function loadState(array $params)
	{
		parent::loadState($params);
		$this->getPaginator()->page = $this->page;
	}

}